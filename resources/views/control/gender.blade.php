@extends('layouts.app')
@section('content')


<div class="container">

  <h2 style="font-size:30px;text-align:center;">Gender</h2>
  <a href="/control" style="color:red;">< Back to Control Panel</a>

  <hr class="hr-divider">
  <div class="row">
    <div class="col-md-4" style="margin:auto;">
      <form i="addForm" class="box form-group" action="{{route('save.gender')}}" method="post" style="margin:auto; width:100%;">
        @csrf

        <label style="margin-top:20px;text-align:center;width:100%;" for="gender_eng" class="">Gender</label>
        <input required autocomplete="off" style="text-align:center;"class="input is-primary form-control" type="text" name="gender_eng" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="gender_dhi" class="faruma">ޖިންސް</label>
        <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="gender_dhi" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="gender_code" class="">CODE</label>
        <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="gender_code" value="">

          <div class="row">
          <div class="col-sm-6">
          <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
          </div>
          <div class="col-sm-6">
          <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
          </div>
          </div>
      </form>
    </div>
  </div>

<hr class="hr-divider">
<table class="table">
  <thead>
    <tr>
      <th>gender</th>
      <th>gender dhi</th>
      <th>gender code</th>
    </tr>
  </thead>
  <tbody>

      @foreach($genders as $gender)
      <tr>
        <td>{{$gender->gender_eng}}</td>
        <td class="faruma">{{$gender->gender_dhi}}</td>
        <td>{{$gender->gender_code}}</td>
    </tr>
      @endforeach

  </tbody>
</table>












</div>

@endsection
