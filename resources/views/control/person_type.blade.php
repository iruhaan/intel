@extends('layouts.app')
@section('content')


<div class="container">
  <h3 style="font-size:30px;text-align:center;">Person Type</h3>
  <hr class="hr-divider">
  <a href="/control" style="color:red;">< Back to Control Panel</a>
  <form i="addForm" class="box form-group" action="{{route('save.person.type')}}" method="post" style="margin:auto; width:30%;">
    @csrf
    <label style="margin-top:20px;text-align:center;width:100%;" for="person_type" class="faruma">މީހާގެ ބާވަތް</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="person_type" value="">

    <label style="margin-top:20px;text-align:center;width:100%;" for="person_type_eng" class="">Type in English</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="person_type_eng" value="">

    <label style="margin-top:20px;text-align:center;width:100%;" for="person_type_code" class="faruma">ކޯޑް</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="person_type_code" value="">

    <div class="row">
      <div class="col-sm-6">
    <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
      </div>
      <div class="col-sm-6">
        <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
    </div>

    </div>
  </form>
<hr>

<table class="table table-hover">
  <thead>
    <tr>
      <th>Person Type | English</th>
      <th class="faruma"style="text-align:right;">މީހާގެ ބާވަތް</th>
      <th style="text-align:center;">Person Type | Code</th>
      <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($person_types as $person_type)
    <tr>
      <td class="">{{$person_type->person_type_eng}}</td>
      <td class="faruma" style="text-align:right;">{{$person_type->person_type}}</td>
      <td style="text-align:center;" class="">{{$person_type->person_type_code}}</td>
      <td style="text-align:center;" class="">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#person{{$person_type->id}}">
          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>
      </td>
    </tr>

    <!-- Modal -->
    <div tabindex="-1" data-backdrop="" class="modal fade" id="person{{$person_type->id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <label></label>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form id="" class="form-group" action="{{route('update.person_type')}}" method="post">
              @csrf
            <label style="width:100%; text-align:right;margin-top:10px;"for="person_type" class="faruma">މީހާގެ ބާވަތް</label>
            <input type="text" name="person_type" value="{{$person_type->person_type}}" class="form-control faruma thaanaKeyboardInput">
            <label style="margin-top:10px;"for="person_type_eng" class="">Person Type | Eng</label>
            <input type="text" name="person_type_eng" value="{{$person_type->person_type_eng}}" class="form-control">
            <label style="margin-top:10px;"for="person_type_code" class="">Person Type | Code</label>
            <input type="text" name="person_type_code" value="{{$person_type->person_type_code}}" class="form-control">
            <input type="hidden" name="person_type_id" value="{{$person_type->id}}">

          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.person_type',['id' => $person_type->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Save changes">

          </div>
          </form>
          </div>

        </div>
      </div>
    </div>





    @endforeach
  </tbody>
</table>



</div>



@endsection
