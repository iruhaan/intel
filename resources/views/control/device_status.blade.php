@extends('layouts.app')
@section('content')


<div class="container">

  <h2 style="font-size:30px;text-align:center;">Device Status</h2>
  <a href="/control" style="color:red;">< Back to Control Panel</a>

  <hr class="hr-divider">
  <div class="row">
    <div class="col-md-4" style="margin:auto;">
      <form i="addForm" class="box form-group" action="{{route('save.device.status')}}" method="post" style="margin:auto; width:100%;">
        @csrf

        <label style="margin-top:20px;text-align:center;width:100%;" for="device_status" class="">Device Status</label>
        <input required autocomplete="off" style="text-align:center;"class="input is-primary form-control" type="text" name="device_status" value="">

          <div class="row">
          <div class="col-sm-6">
          <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
          </div>
          <div class="col-sm-6">
          <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
          </div>
          </div>
      </form>
    </div>
  </div>

<hr class="hr-divider">
<table class="table is-hoverable">
  <thead>
    <tr>
      <th>DEVICE TYPE</th>
      <th style="TEXT-ALIGN:CENTER;">MORE</th>
    </tr>
  </thead>
  <tbody>

      @foreach($device_statuses as $type)
      <tr>
        <td>{{$type->device_status}}</td>
        <td style="text-align:center;" class="">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#person{{$type->id}}">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </button>
        </td>
    </tr>



    <!-- Modal -->
    <div tabindex="-1" data-backdrop="" class="modal fade" id="person{{$type->id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <label></label>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form id="" class="form-group" action="{{route('update.device.status')}}" method="post">
              @csrf
            <label style="margin-top:10px;"for="device_status" class="">Device Status</label>
            <input type="text" name="device_status" value="{{$type->device_status}}" class="form-control">
            <input type="hidden" name="id" value="{{$type->id}}">

          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.device.status',['id' => $type->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Save changes">

          </div>
          </form>
          </div>

        </div>
      </div>
    </div>



      @endforeach

  </tbody>
</table>












</div>

@endsection
