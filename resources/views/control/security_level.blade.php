@extends('layouts.app')
@section('content')


<div class="container">


  <h2 style="font-size:30px;text-align:center;">Security Level</h2>
  <hr class="hr-divider">
  <a href="/control" style="color:red;">< Back to Control Panel</a>
  <form i="addForm" class="box form-group" action="{{route('save.security.level')}}" method="post" style="margin:auto; width:30%;">
    @csrf
    <label style="margin-top:20px;text-align:center;width:100%;" for="security_level_dhi" class="faruma">ސަލާމަތީ ގިންތި</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="security_level_dhi" value="">
    <label style="margin-top:20px;text-align:center;width:100%;" for="security_level_eng" class="">Security Level in English</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="security_level_eng" value="">
    <label style="margin-top:20px;text-align:center;width:100%;" for="security_level_code" class="">level Code</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="security_level_code" value="">
      <div class="row">
      <div class="col-sm-6">
      <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
      </div>
      <div class="col-sm-6">
      <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
      </div>
      </div>
  </form>



  <hr>


<table class="table is-hoverable is-fullwidth" style="background:rgba(171, 178, 185, 0.2);">
  <thead>
    <tr>
    <th>Security Level | English</th>
    <th class="faruma"style="text-align:right;">ސަލާމަތީ ގިންތި</th>
    <th>Level Code</th>
    <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($security_levels as $security_level)
    <tr>
      <td class="" style="text-align:left;">{{$security_level->security_level_eng}}</td>
      <td class="faruma" style="text-align:right;">{{$security_level->security_level_dhi}}</td>
      <td class="" style="text-align:right;">{{$security_level->security_level_code}}</td>
      <td style="text-align:center;" class="">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$security_level->id}}">
      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      </button>
      </td>
    </tr>







    <!-- Modal -->
    <div data-backdrop="" class="modal fade" id="modal{{$security_level->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <label></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
          <form id="" class="form-group" action="{{route('update.security.level')}}" method="post">
            @csrf
          <label style="width:100%; text-align:right;margin-top:10px;"for="security_level_dhi" class="faruma">ސަލާމަތީ ގިންތި</label>
          <input type="text" name="security_level_dhi" value="{{$security_level->security_level_dhi}}" class="form-control faruma thaanaKeyboardInput">
          <label style="margin-top:10px;"for="security_level_eng" class="">Security Level | Eng</label>
          <input type="text" name="security_level_eng" value="{{$security_level->security_level_eng}}" class="form-control">
          <label style="margin-top:10px;"for="security_level_code" class="">Level Code | Eng</label>
          <input type="text" name="security_level_code" value="{{$security_level->security_level_code}}" class="form-control">
          <input type="hidden" name="id" value="{{$security_level->id}}">
          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.security.level',['id' => $security_level->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save changes">
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>





    @endforeach
  </tbody>
</table>

</div>

@endsection
