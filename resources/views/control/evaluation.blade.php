@extends('layouts.app')
@section('content')


<div class="container">

  <h2 style="font-size:30px;text-align:center;">Evaluations</h2>
  <a href="/control" style="color:red;">< Back to Control Panel</a>

  <hr class="hr-divider">
  <div class="row">
    <div class="col-md-6">
      <form i="addForm" class="box form-group" action="{{route('save.information.evaluation')}}" method="post" style="margin:auto; width:100%;">
        @csrf

        <label style="margin-top:20px;text-align:center;width:100%;" for="information_grade" class="">Information Grade</label>
        <input required autocomplete="off" style="text-align:center; text-transform:uppercase;"class="input is-primary form-control" type="text" name="information_grade" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="grade_detail_heading" class="faruma">އިންފޮމޭޝަން ގްރޭޑް ސުރުޙީ | Information Heading</label>
        <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="grade_detail_heading" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="grade_detail" class="faruma">ތަފްސީލް | Description</label>
        <textarea rows="4" type="text" name="grade_detail" class="input is-primary form-control faruma thaanaKeyboardInput">
        </textarea>

          <div class="row">
          <div class="col-sm-6">
          <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
          </div>
          <div class="col-sm-6">
          <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
          </div>
          </div>
      </form>
    </div>

    <div class="col-md-6">
      <form i="addForm" class="box form-group" action="{{route('save.source.evaluation')}}" method="post" style="margin:auto; width:100%;">
        @csrf

        <label style="margin-top:20px;text-align:center;width:100%;" for="source_grade" class="">Source Grade</label>
        <input required autocomplete="off" style="text-align:center; text-transform:uppercase;"class="input is-primary form-control" type="text" name="source_grade" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="grade_detail_heading" class="faruma">ސޯސް ގްރޭޑިން ސުރުޙީ | Source Grading Heading</label>
        <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="grade_detail_heading" value="">

        <label style="margin-top:20px;text-align:center;width:100%;" for="grade_detail" class="faruma">ތަފްސީލް | Description</label>
        <textarea rows="4" type="text" name="grade_detail" class="input is-primary form-control faruma thaanaKeyboardInput">
        </textarea>

          <div class="row">
          <div class="col-sm-6">
          <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
          </div>
          <div class="col-sm-6">
          <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
          </div>
          </div>
      </form>
    </div>
  </div>





<div class="row">
<div class="col-md-6">
  <hr class="hr-divider">
  <h2 style="text-align:center;font-size:22px;">Information Grading</h2>
  <hr class="hr-divider">
  <!-- Information Table -->
  <table class="table is-hoverable is-fullwidth" style="background:rgba(171, 178, 185, 0.2);">
      <thead>
        <tr>
        <th>Grade</th>
        <th class=""style="text-align:right;">Heading</th>
        <th class=""style="text-align:right;">Description</th>
        <th style="text-align:center;">Action</th>
        </tr>
      </thead>
    <tbody>
      @foreach($information_evaluations as $information_evaluation)
      <tr>
        <td class="" style="text-align:left;">{{$information_evaluation->information_grade}}</td>
        <td class="faruma" style="text-align:right;">{{$information_evaluation->grade_detail_heading}}</td>
        <td class="faruma" style="text-align:right;">{{$information_evaluation->grade_detail}}</td>
        <td style="text-align:center;" class="">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$information_evaluation->id}}">
        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>
        </td>
      </tr>
  <!-- Modal -->
  <div data-backdrop="" class="modal fade" id="modal{{$information_evaluation->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <label></label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <form id="" class="form-group" action="{{route('update.information.evaluation')}}" method="post">
          @csrf
        <label style="width:100%; text-align:right;margin-top:10px;"for="information_grade" class="">Information Grade</label>
        <input style="text-transform: uppercase;" type="text" name="information_grade" value="{{$information_evaluation->information_grade}}" class="form-control">

        <label style="width:100%; text-align:right;margin-top:10px;"for="grade_detail_heading" class="faruma">Information Grade Heading</label>
        <input type="text" name="grade_detail_heading" value="{{$information_evaluation->grade_detail_heading}}" class="form-control faruma thaanaKeyboardInput">

        <label style="width:100%; text-align:right;margin-top:10px;"for="grade_detail" class="faruma">Information Grade Detail</label>
        <textarea type="text" name="grade_detail" class="form-control faruma thaanaKeyboardInput">
          {{$information_evaluation->grade_detail}}
        </textarea>

        <input type="hidden" name="id" value="{{$information_evaluation->id}}">
        <div class="modal-footer">
          <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.information.evaluation',['id' => $information_evaluation->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save changes">
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

@endforeach
</tbody>
</table>
</div>
<!-- Information Table -->
<div class="col-md-6">
  <hr class="hr-divider">
  <h2 style="text-align:center;font-size:22px;">Source Grading</h2>
  <hr class="hr-divider">
  <!-- Information Table -->
  <table class="table is-hoverable is-fullwidth" style="background:rgba(171, 178, 185, 0.2);">
      <thead>
        <tr>
        <th>Grade</th>
        <th class=""style="text-align:right;">Heading</th>
        <th class=""style="text-align:right;">Description</th>
        <th style="text-align:center;">Action</th>
        </tr>
      </thead>
    <tbody>
      @foreach($source_evaluations as $source_evaluation)
      <tr>
        <td class="" style="text-align:left;text-transform: uppercase;">{{$source_evaluation->source_grade}}</td>
        <td class="faruma" style="text-align:right;">{{$source_evaluation->grade_detail_heading}}</td>
        <td class="faruma" style="text-align:right;">{{$source_evaluation->grade_detail}}</td>
        <td style="text-align:center;" class="">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#source{{$source_evaluation->id}}">
        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>
        </td>
      </tr>
  <!-- Modal -->
  <div data-backdrop="" class="modal fade" id="source{{$source_evaluation->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <label></label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <form id="" class="form-group" action="{{route('update.source.evaluation')}}" method="post">
          @csrf
        <label style="width:100%; text-align:right;margin-top:10px;"for="source_grade" class="">Source Grade</label>
        <input style="text-transform: uppercase;" type="text" name="source_grade" value="{{$source_evaluation->source_grade}}" class="form-control">

        <label style="width:100%; text-align:right;margin-top:10px;"for="grade_detail_heading" class="faruma">Source Grade Heading</label>
        <input type="text" name="grade_detail_heading" value="{{$source_evaluation->grade_detail_heading}}" class="form-control faruma thaanaKeyboardInput">

        <label style="width:100%; text-align:right;margin-top:10px;"for="grade_detail" class="faruma">Source Grade Detail</label>
        <textarea type="text" name="grade_detail" class="form-control faruma thaanaKeyboardInput">
          {{$source_evaluation->grade_detail}}
        </textarea>

        <input type="hidden" name="id" value="{{$source_evaluation->id}}">
        <div class="modal-footer">
          <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.source.evaluation',['id' => $source_evaluation->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save changes">
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

@endforeach
</tbody>
</table>
</div>
</div>










</div>

@endsection
