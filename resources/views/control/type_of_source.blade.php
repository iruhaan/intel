@extends('layouts.app')
@section('content')


<div class="container">


  <h2 style="font-size:30px;text-align:center;">Source Type</h2>
  <hr class="hr-divider">
  <a href="/control" style="color:red;">< Back to Control Panel</a>
  <form i="addForm" class="box form-group" action="{{route('save.source.type')}}" method="post" style="margin:auto; width:30%;">
    @csrf
    <label style="margin-top:20px;text-align:center;width:100%;" for="type_of_source" class="faruma">ސޯސް ބާވަތް</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="type_of_source" value="">
    <label style="margin-top:20px;text-align:center;width:100%;" for="type_of_source_eng" class="">Type of Source | Eng</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="type_of_source_eng" value="">
    <label style="margin-top:20px;text-align:center;width:100%;" for="code" class="">code</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="code" value="">
      <div class="row">
      <div class="col-sm-6">
      <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
      </div>
      <div class="col-sm-6">
      <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
      </div>
      </div>
  </form>



  <hr>


<table class="table is-hoverable is-fullwidth" style="background:rgba(171, 178, 185, 0.2);">
  <thead>
    <tr>
    <th class="faruma"style="text-align:right;">ސޯސްގެ ބަވަތް</th>
    <th class=""style="text-align:center;">Code</th>
    <th class=""style="text-align:center;">Type of Source</th>
    <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($source_types as $source)
    <tr>
      <td class="faruma" style="text-align:right;">{{$source->type_of_source}}</td>
      <td class="" style="text-align:center;">{{$source->code}}</td>
      <td class="" style="text-align:center;">{{$source->type_of_source_eng}}</td>
      <td style="text-align:center;" class="">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$source->id}}">
      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      </button>
      </td>
    </tr>







    <!-- Modal -->
    <div data-backdrop="" class="modal fade" id="modal{{$source->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <label></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
          <form id="" class="form-group" action="{{route('update.source.type')}}" method="post">
            @csrf
          <label style="width:100%; text-align:right;margin-top:10px;"for="source_type" class="faruma">ސޯސް ބާވަތް</label>
          <input type="text" name="type_of_source" value="{{$source->type_of_source}}" class="form-control faruma thaanaKeyboardInput">
          <label style="margin-top:10px;"for="type_of_source_eng" class="">Source Type in English</label>
          <input type="text" name="type_of_source_eng" value="{{$source->type_of_source_eng}}" class="form-control">
          <label style="margin-top:10px;"for="code" class="">Code</label>
          <input type="text" name="code" value="{{$source->code}}" class="form-control">
          <input type="hidden" name="id" value="{{$source->id}}">
          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.source.type',['id' => $source->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save changes">
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>





    @endforeach
  </tbody>
</table>



</div>

@endsection
