@extends('layouts.app')
@section('content')


<div class="container">
  <h3 style="font-size:30px;text-align:center;">Gange Names</h3>
  <hr class="hr-divider">
  <a href="/control" style="color:red;">< Back to Control Panel</a>
  <form i="addForm" class="box form-group" action="{{route('save.gang.name')}}" method="post" style="margin:auto; width:30%;">
    @csrf
    <label style="margin-top:20px;text-align:center;width:100%;" for="gang_name_dhi" class="faruma">ގޭންގެ</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="gang_name_dhi" value="">

    <label style="margin-top:20px;text-align:center;width:100%;" for="gang_name_eng" class="">Gang Name in English</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="gang_name_eng" value="">

    <div class="row">
      <div class="col-sm-6">
    <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
      </div>
      <div class="col-sm-6">
        <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
    </div>

    </div>
  </form>
<hr>

<table class="table table-hover">
  <thead>
    <tr>
      <th>Gang Name | English</th>
      <th class="faruma"style="text-align:right;">ގޭންގް ނަން</th>
      <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($gangnames as $gang)
    <tr>
      <td class="" style="text-align:left;">{{$gang->gang_name_eng}}</td>
      <td class="faruma" style="text-align:right;">{{$gang->gang_name_dhi}}</td>
      <td style="text-align:center;" class="">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#gang{{$gang->id}}">
          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>
      </td>
    </tr>

    <!-- Modal -->
    <div data-backdrop="" class="modal fade" id="gang{{$gang->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <label></label>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form id="" class="form-group" action="{{route('update.gang.name')}}" method="post">
              @csrf
            <label style="width:100%; text-align:right;margin-top:10px;"for="gang_name_dhi" class="faruma">ގޭންގް ނަން</label>
            <input type="text" name="gang_name_dhi" value="{{$gang->gang_name_dhi}}" class="form-control faruma thaanaKeyboardInput">
            <label style="margin-top:10px;"for="gang_name_eng" class="">Gang Name | Eng</label>
            <input type="text" name="gang_name_eng" value="{{$gang->gang_name_eng}}" class="form-control">
            <input type="hidden" name="id" value="{{$gang->id}}">

          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.gang.name',['id' => $gang->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Save changes">

          </div>
          </form>
          </div>

        </div>
      </div>
    </div>





    @endforeach
  </tbody>
</table>



</div>

@endsection
