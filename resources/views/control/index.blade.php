@extends('layouts.app')
@section('content')


<div class="container">
  <h2 style="text-align:center;font-size:30px;color:#FE3B2F;">Control Panel</h2>
  <hr class="hr-divider">


    <div class="div-control box">
      <h4 style="margin:10px;text-align:center;">Subject Setting</h4>
      <ul class="ul-control">
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('person.type')}}">Person Type</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('gang.name')}}">Gang Names</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('country')}}">Country</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('evaluation')}}">Evaluation</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('security.level')}}">Security Level</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('source.type')}}">Source Type</a></li>
            <li><a style="width:100%;text-decoration:none;" class="button is-danger" href="{{route('gender')}}">gender</a></li>
      </ul>
    </div>

    <div class="div-control box">
      <h4 style="margin:10px;text-align:center;">Contact and Device Setting</h4>
      <ul class="ul-control">
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('device.type')}}">Device Type</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('device.status')}}">Device Status</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('contact.status')}}">Contact Status</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('contact.type')}}">Contact Type</a></li>
            <li><a style="text-decoration:none;" class="button is-danger btn-cont" href="{{route('device.brand')}}">Device Brand</a></li>
      </ul>
    </div>








</div>

@endsection
