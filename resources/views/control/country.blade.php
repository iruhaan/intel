@extends('layouts.app')
@section('content')


<div class="container">


  <h2 style="font-size:30px;text-align:center;">Countries</h2>
  <hr class="hr-divider">
  <a href="/control" style="color:red;">< Back to Control Panel</a>
  <form i="addForm" class="box form-group" action="{{route('save.country')}}" method="post" style="margin:auto; width:30%;">
    @csrf
    <label style="margin-top:20px;text-align:center;width:100%;" for="country_dhi" class="faruma">ޤައުމު</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary thaanaKeyboardInput faruma form-control" type="text" name="country_dhi" value="">
    <label style="margin-top:20px;text-align:center;width:100%;" for="country_eng" class="">Country in English</label>
    <input required autocomplete="off" style="text-align:center"class="input is-primary form-control" type="text" name="country_eng" value="">
      <div class="row">
      <div class="col-sm-6">
      <input style="width:100%; margin-top:20px;"class="btn btn-success"type="submit" name="Save" value="Save">
      </div>
      <div class="col-sm-6">
      <a href="/control" style="width:100%; margin-top:20px;"class="btn btn-danger">Cancel</a>
      </div>
      </div>
  </form>



  <hr>


<table class="table is-hoverable is-fullwidth" style="background:rgba(171, 178, 185, 0.2);">
  <thead>
    <tr>
    <th>Country | English</th>
    <th class="faruma"style="text-align:right;">ޤައުމު</th>
    <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($countries as $country)
    <tr>
      <td class="" style="text-align:left;">{{$country->country_eng}}</td>
      <td class="faruma" style="text-align:right;">{{$country->country_dhi}}</td>
      <td style="text-align:center;" class="">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#country{{$country->id}}">
      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      </button>
      </td>
    </tr>







    <!-- Modal -->
    <div data-backdrop="" class="modal fade" id="country{{$country->id}}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <label></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
          <form id="" class="form-group" action="{{route('update.country')}}" method="post">
            @csrf
          <label style="width:100%; text-align:right;margin-top:10px;"for="country_dhi" class="faruma">ޤައުމު</label>
          <input type="text" name="country_dhi" value="{{$country->country_dhi}}" class="form-control faruma thaanaKeyboardInput">
          <label style="margin-top:10px;"for="country_eng" class="">Country | Eng</label>
          <input type="text" name="country_eng" value="{{$country->country_eng}}" class="form-control">
          <input type="hidden" name="id" value="{{$country->id}}">
          <div class="modal-footer">
            <a title="Delete this entire record" class="btn"style="float:left;"href="{{route('delete.country',['id' => $country->id])}}" class=""><i style="color:red;" class="fa fa-trash fa-2x"></i></a>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save changes">
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>





    @endforeach
  </tbody>
</table>



</div>

@endsection
