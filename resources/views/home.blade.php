@extends('layouts.app')
<link rel="stylesheet" href="/css/home.scss">
@section('content')
<div class="container">


    <a href="{{route('add.subject')}}"><button class="custom-btn btn-13">Add Subject</button></a>

    <h1 class="waheedh" style="text-align:center;">ސަބްޖެކްޓް ރަޖިސްޓްރީ</h1>
    <hr class="hr-divider">



    <table class="faruma table table-hover table-striped" id="dataTables" style="font-size:110%;color:black;background-color:rgb(0, 0, 0,3%);">
      <thead>
        <tr>
          <th style="text-align:center;">އައިޑީ</th>
          <th style="text-align:right;">ނަމާއި އެޑްރެސް</th>
          <th style="text-align:center;">އައިޑެންޓިޓީ</th>
          <th style="text-align:center;">ފޯން</th>
          <th style="text-align:right;">ވަނަން</th>
          <th style="text-align:center;">މީހާގެ ބާވަތް</th>
          <th style="text-align:center;"></th>
        </tr>
      </thead>
      <tbody>
        @foreach($subjects as $subject)
        <tr>
          <td style="text-align:center;">{{$subject->id}}</td>
          <td style="text-align:right;">{{$subject->full_name}} {{$subject->address}}</td>
          <td style="text-align:center;">{{$subject->NID}}</td>
          <td style="text-align:center;">{{$subject->contact}}</td>
          <td style="text-align:right;">{{$subject->nick_name}}</td>
          <td style="text-align:center;">{{$subject->person_type->person_type}}</td>
          <td style="text-align:center;"><a href="{{route('subject',['id' => $subject->id])}}" title="More Details" class="btn btn-success"><i class="fa fa-external-link" aria-hidden="true"></i></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>


<script type="text/javascript">
$(document).ready(function () {
    $('#dataTables').DataTable();
    $('div.dataTables_filter input').addClass('thaanaKeyboardInput');
})
</script>



@endsection
