@extends('layouts.app')
@section('content')



<h1 style="text-align:center;" class="waheedh">ސަބްޖެކް / މީހުން ރަޖިސްޓަރކުރުން</h1>
<hr class="hr-divider">

<form class="form-group" action="{{route('save.subject')}}" method="post">
@csrf

<div class="row box">
    <div class="col-2">
      <label for="NID" class="dhi-lbl">އައިޑީ ކާޑް ނަންބަރ</label>
      <input autocomplete="off" tyle="text-transform: capitalize;text-align:center;"type="text" name="NID" value="" class="form-control">
    </div>
    <div class="col-3">
      <label for="NID" class="dhi-lbl">މިހާރު ދިރިއުޅޭ އެޑްރެސް</label>
      <input autocomplete="off" type="text" name="present_address" value="" class="form-control thaanaKeyboardInput faruma">
    </div>
    <div class="col-4">
      <label for="NID" class="dhi-lbl">އެޑްރެސް | އަތޮޅާއި ރަށާއެކު</label>
      <input autocomplete="off" type="text" name="address" value="" class="thaanaKeyboardInput faruma form-control">
    </div>
    <div class="col-3">
      <label for="NID" class="dhi-lbl">ފުރިހަމަ ނަން</label>
      <input autocomplete="off" required type="text" name="full_name" value="" class="thaanaKeyboardInput faruma form-control">
    </div>
  </div>
<div class="row box">
    <div class="col-2">
      <label for="NID" class="dhi-lbl">އުފަން ތާރީޚް</label>
      <input autocomplete="off" style="text-align:center;" type="text" name="DOB" value="" class="datepicker form-control">
    </div>
    <div class="col-3">
      <label for="contact" class="dhi-lbl">ގުޅޭނެ ނަންބަރު، ނަންބަރު ތައް</label>
      <input autocomplete="off" style="text-align:center;" type="text" name="contact" value="" class="form-control">
    </div>
    <div class="col-4">
      <label for="nick_name" class="dhi-lbl">ވަނަން</label>
      <input autocomplete="off" style="text-align:center;" type="text" name="nick_name" value="" class="faruma form-control thaanaKeyboardInput">
    </div>
    <div class="col-3">
      <label for="nick_name" class="dhi-lbl">ޖިންސް</label>
      <select required style="text-align:center;" type="text" name="gender_id" value="" class="faruma form-control">
        <option  value="" selected class="faruma">--ޖިންސް ނަންގަވާ--</option>
        @foreach($genders as $gender)
        <option value="{{$gender->id}}">{{$gender->gender_dhi}}</option>
        @endforeach
    </select>
    </div>

</div>



    <div class="row box">

      <div class="col-3">
        <label for="nick_name" class="dhi-lbl">ނިސްބަތްވާ ގޭންގް - އެނގޭނަމަ</label>
        <select style="text-align:center;" type="text" name="gang_id" value="" class="faruma form-control">
          <option disabled selected class="faruma"> ނަންގަވާ </option>
          @foreach($gangs as $gang)
          <option value="{{$gang->id}}">{{$gang->gang_name_dhi}}</option>
          @endforeach
      </select>
      </div>
      <div class="col-3">
        <label for="nick_name" class="dhi-lbl">ސަލާމަތީ ގިންތި</label>
        <select style="text-align:center;" type="text" name="security_level_id" value="" class="faruma form-control">
          <option disabled selected class="faruma">ނަންގަވާ</option>
          @foreach($security_levels as $level)
          <option value="{{$level->id}}">{{$level->security_level_dhi}}</option>
          @endforeach
      </select>
      </div>
      <div class="col-3">
        <label for="nick_name" class="dhi-lbl">ޤައުމު</label>
        <select style="text-align:center;" type="text" name="country_id" value="" class="faruma form-control">
          <option disabled selected class="faruma">-ޤައުމު ނަންގަވާ--</option>
          @foreach($countries as $country)
          <option value="{{$country->id}}">{{$country->country_dhi}}</option>
          @endforeach
      </select>
      </div>
      <div class="col-3">
        <label for="nick_name" class="dhi-lbl">މީހާގެ ބާވަތް</label>
        <select required style="text-align:center;" type="text" name="person_type_id" value="" class="faruma form-control">
          <option selected class="faruma" value="">ނަންގަވާ</option>
          @foreach($person_types as $person)
          <option value="{{$person->id}}">{{$person->person_type}}</option>
          @endforeach
      </select>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-2">
        <input class="btn btn-success"style="width:100%;"type="submit" name="" value="Save">
      </div>
      <div class="col-2">
        <a href="/home"class="btn btn-danger"style="width:100%;"type="submit" name="" value="Cancel">Cancel</a>
      </div>
      <div class="col-2">
        <input class="btn btn-info"style="width:100%;"type="reset" name="" value="Reset">
      </div>
    </div>



</form>

<!-- <label for="inp" class="inp">
  <input type="text" id="inp" placeholder="&nbsp;">
  <span class="label">Label</span>
  <span class="focus-bg"></span>
</label> -->


@endsection
