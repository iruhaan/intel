@extends('layouts.app')
@section('content')

<a class="firacode btn"href="/home">Back to Subject Registry</a>
<img class="profile_picture" src="\icon\female.jpg" alt="">
<h1 style="margin:10px;text-align:center;" class="waheedh">{{$subject->full_name}}</h1>
<hr class="hr-divider">
<h3 style="margin:10px;text-align:center;" class="waheedh">{{$subject->address}} | {{$subject->age()}}އ | {{$subject->NID}}</h3>

<table class="waheedh" style="font-size:22px;margin: 0 auto;">
  <tr>
    <td class="profile_top-border-right waheedh">އުފަން ތާރީޚް</td>
    <td class="profile_top-border-left waheedh">{{$subject->DOB}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">ގުޅޭނެ ނަންބަރ</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->contact}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">ވަނަން</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->nick_name}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">މީހާގެ ބާވަތް</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->person_type->person_type}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">ޤައުމު</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->country->country_dhi}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">ނިސްބަތްވާ ގޭންގް</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->gang->gang_name_dhi}}</td>
  </tr>
  <tr>
    <td class="profile_middle-border-right waheedh">ޖިންސް</td>
    <td class="profile_middle-border-left waheedh" style="text-align:right">{{$subject->gender->gender_dhi}}</td>
  </tr>
  <tr>
    <td class="profile_buttom-border-right waheedh">ސަލާމަތީ ގިންތި</td>
    <td class="profile_buttom-border-left waheedh" style="text-align:right">{{$subject->security_level->security_level_dhi}}</td>
  </tr>
</table>

<h2 class="waheedh" style="margin-top:50px;text-align:center;">ހިމެނޭ ހާދިޘާތައް | ގުޅުންހުރި ކަންކަން</h2>
<hr class="hr-divider">
<table id="event" class="table-striped table table-hover faruma" style="color:#000; font-size:100%;border-top: solid 4px #2ECC71; border-radius:10px;">
  <thead>
    <tr>
      <td class="waheedh" style="text-align:center;">ތާރީޚް</td>
      <td class="waheedh" style="text-align:right;">ހަރަކާތް | މަޢުލޫމާތު</td>
      <td class=""></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center;" class="firacode">dsd</td>
      <td style="text-align:right;">Tesqt</td>
      <td style="text-align:center;"><a href="" title="More Details" class="btn btn-success"><i class="fa fa-external-link" aria-hidden="true"></i></a></td>
    </tr>

  </tbody>
</table>


<!-- Devices -->
<h2 id="deviceTag" class="firacode" style="margin-top:50px;text-align:center;">Registered Devices</h2>
<button  style="float:left; margin-bottom:10px;" type="button" class="firacode btn btn-danger" data-toggle="modal" data-target="#addDevice">Add Device</button>
<hr class="hr-divider">

<table id="device" class="table-striped table table-hover firacode" style="color:#000;border-top: solid 4px #2ECC71; border-radius:10px;">
  <thead>
    <tr>
      <td style="text-align:center;">Date</td>
      <td style="text-align:center;">Device Type</td>
      <td style="text-align:center;">Brand</td>
      <td style="text-align:center;">Model</td>
      <td style="text-align:center;">IMEI 1</td>
      <td style="text-align:center;">IMEI 2</td>
      <td style="text-align:center;">Serial NO</td>
      <td style="text-align:center;">Current Status</td>
      <td style="text-align:center;">Status Date</td>
      <td style="text-align:center;"></td>
    </tr>
  </thead>
  @foreach($subject->device_registry as $device)
  <tbody>

    <tr>
      <td style="text-align:center;">{{$device->date}}</td>
      <td style="text-align:center;">{{$device->device_type->device_type}}</td>
      <td style="text-align:center;">{{$device->device_brand->brand}}</td>
      <td style="text-align:center;">{{$device->model}}</td>
      <td style="text-align:center;">{{$device->IMEI1}}</td>
      <td style="text-align:center;">{{$device->IMEI2}}</td>
      <td style="text-align:center;">{{$device->serial_no}}</td>
      <td style="text-align:center;">{{$device->device_status->device_status}}</td>
      <td style="text-align:center;">{{$device->status_date}}</td>
      <td style="text-align:center;"><a title="More Details" class="btn btn-success" data-toggle="modal" data-target="#updateDevice{{$device->id}}"><i class="fa fa-external-link" aria-hidden="true"></i></a></td>
      <div>
      <!-- ModalFor Update Device -->
        <form class="form-group" action="{{route('update.device.registry',['id' => $device->id])}}" method="post">
          @csrf
        <div  data-backdrop="false" style="background-color: rgba(169, 50, 38, 0.7);" class="firacode modal fade" id="updateDevice{{$device->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#D5D8DC;color:black;border-top: solid 4px #2ECC71;">
              <h5 class="modal-title" id="exampleModalCenterTitle">Update Device</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body firacode">
              <div class="row">
                <div class="col-6">
                  <label for="date">Date</label>
                  <input autocomplete="off" type="text" name="date" value="{{$device->date}}" class="form-control datepicker firacode">
                </div>
                <div class="col-6">
                  <label for="date">Model</label>
                  <input autocomplete="off" type="text" name="model" value="{{$device->model}}" class="firacode form-control">
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-6">
                  <label for="">Type of Device</label>
                  <select required class="form-control firacode" name="device_type_id">
                    @foreach($device_types as $type)
                    @if($device->device_type_id == $type->id)
                    <option selected value="{{$type->id}}">{{$type->device_type}}</option>
                    @else
                    <option value="{{$type->id}}">{{$type->device_type}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <div class="col-6">
                  <label for="device_brand_id">Brand</label>
                  <select required class="form-control firacode" name="device_brand_id">
                    @foreach($device_brands as $brand)
                    @if($device->device_brand_id == $brand->id)
                    <option selected value="{{$brand->id}}">{{$brand->brand}}</option>
                    @else
                    <option value="{{$brand->id}}">{{$brand->brand}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <br>
              <label for="serial_no">Serial NO</label>
              <input autocomplete="off" type="text" name="serial_no" value="{{$device->serial_no}}" class="firacode form-control">
              <br>
              <div class="row">
                <div class="col-6">
                  <label for="imei1">IMEI 1</label>
                  <input autocomplete="off" type="text" name="IMEI1" class="firacode form-control" value="{{$device->IMEI1}}">
                </div>
                <div class="col-6">
                  <label for="imei2">IMEI 2</label>
                  <input autocomplete="off" type="text" name="IMEI2" class="firacode form-control" value="{{$device->IMEI2}}">
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-6">
                  <label for="imei1">Status Date</label>
                  <input autocomplete="off" type="text" name="status_date" class="datepicker firacode form-control" value="{{$device->status_date}}">
                </div>
                <div class="col-6">
                  <label for="device_status_id">Current Status</label>
                  <select required class="form-control firacode" name="device_status_id">
                    @foreach($device_statuses as $status)
                    @if($device->device_status_id == $status->id)
                    <option selected value="{{$status->id}}">{{$status->device_status}}</option>
                    @else
                    <option value="{{$status->id}}">{{$status->device_status}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <input type="hidden" name="subjects_id" value="{{$subject->id}}">

            </div>
            <div class="modal-footer" style="color:white; background:#D5D8DC;border-bottom: solid 4px #2ECC71;">
              <a class="firacode btn btn-danger" data-dismiss="modal">Close</a>
              <button type="reset" class="firacode btn btn-primary">Clear</button>
              <button type="submit" class="firacode btn btn-success">Update Records</button>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
    </tr>
  </tbody>
@endforeach
</table>


<!-- Contact -->
<h2 class="firacode" style="margin-top:50px;text-align:center;">Registered Contacts</h2>
<button  style="float:left; margin-bottom:10px;" type="button" class="firacode btn btn-danger" data-toggle="modal" data-target="#addContact">Add Contact</button>
<hr class="hr-divider">
<table id="contact" class="table-striped table table-hover firacode" style=" font-size:100%;border-top: solid 4px #2ECC71; border-radius:10px;">
  <thead>
    <tr>
      <td style="text-align:center;">Date</td>
      <td style="text-align:center;">Contact Type</td>
      <td style="text-align:center;">Contact</td>
      <td style="text-align:center;">Attached Device</td>
      <td style="text-align:center;">Current Status</td>
      <td style="text-align:center;">Status Date</td>
      <td style="text-align:center;"></td>
    </tr>
  </thead>
  <tbody>
    @foreach($subject->contact_registry as $contact)
    <tr>
      <td style="text-align:center;">{{$contact->date}}</td>
      <td style="text-align:center;">{{$contact->contact_type->contact_type}}</td>
      <td style="text-align:center;">{{$contact->contact}}</td>
      <td style="text-align:center;">{{$contact->device_registry->device_brand->brand}} - {{$contact->device_registry->model}}</td>
      <td style="text-align:center;">{{$contact->contact_status->contact_status}}</td>
      <td style="text-align:center;">{{$contact->contact_status_date}}</td>
      <td style="text-align:center;"><a title="More Details" class="btn btn-success" data-toggle="modal" data-target="#updateContact{{$contact->id}}"><i class="fa fa-external-link" aria-hidden="true"></i></a></td>
    </tr>
    <div>
    <!-- ModalFor Update Contact -->
      <form class="form-group" action="{{route('update.contact',['id' => $contact->id])}}" method="post">
        @csrf
      <div  data-backdrop="false" style="background-color: rgba(169, 50, 38, 0.7);" class="firacode modal fade" id="updateContact{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#D5D8DC;color:black;border-top: solid 4px #2ECC71;">
            <h5 class="modal-title" id="exampleModalCenterTitle">Update Contact</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body firacode">
            <div class="row">
              <div class="col-6">
                <label for="date">Date</label>
                <input type="text" name="date" value="{{$contact->date}}" class="firacode form-control datepicker" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="contact_type_id">Contact Type</label>
                <select class="firacode form-control" name="contact_type_id">
                  @foreach($contact_types as $contact_type)
                  @if($contact->contact_type_id == $contact_type->id)
                  <option selected value="{{$contact_type->id}}">{{$contact_type->contact_type}}</option>
                  @else
                  <option value="{{$contact_type->id}}">{{$contact_type->contact_type}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-12">
                <label for="contact">Contact</label>
                <input type="text" name="contact" value="{{$contact->contact}}" class="firacode form-control" autocomplete="off">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-12">
                <label for="device_registry_id">Assaigned Device</label>
                <select class="form-control firacode" name="device_registry_id">
                  @foreach($subject->contact_registry as $device)
                  @if($device->device_registry_id == $contact->device_registry_id)
                    <option class="firacode"selected value="{{$device->id}}">{{$device->device_registry->device_brand->brand}} - {{$device->device_registry->model}}</option>
                    @else
                    <option class="firacode"value="">No Device</option>
                    <option class="firacode"value="{{$device->id}}">{{$device->device_registry->device_brand->brand}} - {{$device->device_registry->model}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-6">
                <label for="contact_status_id">Contact Status</label>
                <select class="firacode- form-control" name="contact_status_id">
                  <option value="">NO STATUS</option>
                  @foreach($contact_statuses as $status)
                  @if($status->id == $contact->contact_status_id)
                  <option selected value="{{$status->id}}">{{$status->contact_status}}</option>
                  @else
                  <option value="{{$status->id}}">{{$status->contact_status}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="col-6">
                <label for="contact_status_date Date">Status Updated Date</label>
                <input autocomplete="off" type="text" name="contact_status_date" value="{{$contact->contact_status_date}}" class="firacode form-control datepicker">
              </div>
            </div>

          <input type="hidden" name="subjects_id" value="{{$subject->id}}">
          </div>
          <div class="modal-footer" style="color:white; background:#D5D8DC;border-bottom: solid 4px #2ECC71;">
            <a class="firacode btn btn-danger" data-dismiss="modal">Close</a>
            <button type="reset" class="firacode btn btn-primary">Clear</button>
            <button type="submit" class="firacode btn btn-success">Update Records</button>
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
    @endforeach
  </tbody>
</table>


<!-- ModalFor add Contact -->
  <form class="form-group" action="{{route('save.contact')}}" method="post">
    @csrf
  <div  data-backdrop="false" style="background-color: rgba(29, 131, 72, 0.6);" class="firacode modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#D5D8DC;color:black;border-top: solid 4px #2ECC71;">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body firacode">
        <div class="row">
          <div class="col-6">
            <label for="date">Date</label>
            <input autocomplete="off" required type="text" name="date" value="" class="firacode form-control datepicker">
          </div>
          <div class="col-6">
            <label for="Contact Type">Contact Type</label>
            <select required class="form-control firacode" name="contact_type_id">
              <option value="" selected>Please Select</option>
              @foreach($contact_types as $type)
              <option class="firacode" value="{{$type->id}}">{{$type->contact_type}}</option>
              @endforeach
            </select>
          </div>
          <br>
        </div>

        <br>

        <div class="row">
          <div class="col-6">
            <label for="contact">Contact</label>
            <input autocomplete="off" required type="text" name="contact" value="" class="firacode form-control">
          </div>
          <div class="col-6">
            <label for="contact">Attached Device</label>
            <select class="firacode form-control" name="device_registry_id">
              <option selected value="">Please Select</option>
              <option value="">No Device</option>
              @foreach($subject->device_registry as $device)
              <option value="{{$device->id}}">{{$device->device_brand->brand}} | {{$device->model}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <input type="hidden" name="subjects_id" value="{{$subject->id}}">

      <br>

      </div>
      <div class="modal-footer" style="color:white; background:#D5D8DC;border-bottom: solid 4px #2ECC71;">
        <a class="firacode btn btn-danger" data-dismiss="modal">Close</a>
        <button type="reset" class="firacode btn btn-primary">Clear</button>
        <button type="submit" class="firacode btn btn-success">Save Records</button>
      </div>
    </div>
  </div>
</div>
</form>



<!-- ModalFor add Device -->
  <form class="form-group" action="{{route('save.device.registry')}}" method="post">
    @csrf
  <div  data-backdrop="false" style="background-color: rgba(29, 131, 72, 0.6);" class="firacode modal fade" id="addDevice" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#D5D8DC;color:black;border-top: solid 4px #2ECC71;">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Device</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body firacode">
        <div class="row">
          <div class="col-6">
            <label for="date">Date</label>
            <input autocomplete="off" type="text" name="date" value="" class="form-control datepicker firacode">
          </div>
          <div class="col-6">
            <label for="date">Model</label>
            <input autocomplete="off" type="text" name="model" value="" class="firacode form-control">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-6">
            <label for="">Type of Device</label>
            <select required class="form-control firacode" name="device_type_id">
              <option value="" selected>Please Select</option>
              @foreach($device_types as $type)
              <option value="{{$type->id}}">{{$type->device_type}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-6">
            <label for="device_brand_id">Brand</label>
            <select required class="form-control firacode" name="device_brand_id">
              <option value="" selected>Please Select</option>
              @foreach($device_brands as $brand)
              <option value="{{$brand->id}}">{{$brand->brand}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <br>
        <label for="serial_no">Serial NO</label>
        <input autocomplete="off" type="text" name="serial_no" value="" class="firacode form-control">
        <br>
        <div class="row">
          <div class="col-6">
            <label for="imei1">IMEI 1</label>
            <input autocomplete="off" type="text" name="IMEI1" class="firacode form-control" value="">
          </div>
          <div class="col-6">
            <label for="imei2">IMEI 2</label>
            <input autocomplete="off" type="text" name="IMEI2" class="firacode form-control" value="">
          </div>
        </div>
        <input type="hidden" name="subjects_id" value="{{$subject->id}}">
        <input type="hidden" name="device_status_id" value="1">
      </div>
      <div class="modal-footer" style="color:white; background:#D5D8DC;border-bottom: solid 4px #2ECC71;">
        <a class="firacode btn btn-danger" data-dismiss="modal">Close</a>
        <button type="reset" class="firacode btn btn-primary">Clear</button>
        <button type="submit" class="firacode btn btn-success">Save Records</button>
      </div>
    </div>
  </div>
</div>
</form>









<script type="text/javascript">
$(document).ready(function () {
    $('#event').DataTable();
    $('div.dataTables_filter input').addClass('thaanaKeyboardInput');
})
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#contact').DataTable();
})
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#device').DataTable();
})
</script>


@endsection
