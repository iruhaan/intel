<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homesearch','HomeController@index')->name('search');
//control Panel
Route::get('/control','controlpanelController@index')->name('control');
Route::get('/control->personType','controlpanelController@personType')->name('person.type');
Route::post('/savepersontype','controlpanelController@savePersonType')->name('save.person.type');
Route::post('/updatepersontype','controlpanelController@updatePersonType')->name('update.person_type');
Route::get('/deletepersontype/{id}','controlpanelController@deletePersonType')->name('delete.person_type');

Route::get('/control->gang','controlpanelController@gangName')->name('gang.name');
Route::get('/deletegang/{id}','controlpanelController@deleteGangName')->name('delete.gang.name');
Route::post('/gangname','controlpanelController@saveGangName')->name('save.gang.name');
Route::post('/updategangname','controlpanelController@updateGangName')->name('update.gang.name');

Route::get('/control->country','controlpanelController@country')->name('country');
Route::get('/country/{id}','controlpanelController@deleteCountry')->name('delete.country');
Route::post('/savecountry','controlpanelController@saveCountry')->name('save.country');
Route::post('/updatecountry','controlpanelController@updateCountry')->name('update.country');

//Evaluation
Route::get('/control->evaluation','controlpanelController@evaluation')->name('evaluation');
Route::post('/saveinformationevaluation','controlpanelController@saveInformationEvaluation')->name('save.information.evaluation');
Route::post('/updateinformationevaluation','controlpanelController@updateInformationEvaluation')->name('update.information.evaluation');
Route::get('/deleteinformationevaluation/{id}','controlpanelController@deleteInformationEvaluation')->name('delete.information.evaluation');

Route::post('/saveinsourecevaluation','controlpanelController@saveSourceEvaluation')->name('save.source.evaluation');
Route::post('/updateinfsourecvaluation','controlpanelController@updateSourceEvaluation')->name('update.source.evaluation');
Route::get('/deleteinfsourecvaluation/{id}','controlpanelController@deleteSourceEvaluation')->name('delete.source.evaluation');

//Security Level
Route::get('/control->Security_level','controlpanelController@security_level')->name('security.level');
Route::post('/savesecurity_level','controlpanelController@saveSecurityLevel')->name('save.security.level');
Route::post('/updatesecurity_level','controlpanelController@updateSecurityLevel')->name('update.security.level');
Route::get('/deletesecurity_level{id}','controlpanelController@deleteSecurityLevel')->name('delete.security.level');

//Source Type
Route::get('/control->source_type','controlpanelController@source_type')->name('source.type');
Route::post('/savesourcetype','controlpanelController@saveSourceType')->name('save.source.type');
Route::post('/updatesourcetype','controlpanelController@updateSourceType')->name('update.source.type');
Route::get('/deletesourcetype{id}','controlpanelController@deleteSourceType')->name('delete.source.type');

//Device Type
Route::get('/control->device_type','controlpanelController@deviceType')->name('device.type');
Route::post('/savedevicetype','controlpanelController@saveDeviceType')->name('save.device.type');
Route::post('/updatedevicetype','controlpanelController@updateDeviceType')->name('update.device.type');
Route::get('/deletedevicetype{id}','controlpanelController@deleteDeviceType')->name('delete.device.type');

//Device Status
Route::get('/control->device_status','controlpanelController@deviceStatus')->name('device.status');
Route::post('/savedevicestatus','controlpanelController@saveDeviceStatus')->name('save.device.status');
Route::post('/updatedevicestatus','controlpanelController@updateDeviceStatus')->name('update.device.status');
Route::get('/deletedevicestatus{id}','controlpanelController@deleteDeviceStatus')->name('delete.device.status');

//Contact Status
Route::get('/control->contact__status','controlpanelController@ContactStatus')->name('contact.status');
Route::post('/savecontact_status','controlpanelController@saveContactStatus')->name('save.contact.status');
Route::post('/updatecontact_status','controlpanelController@updateContactStatus')->name('update.contact.status');
Route::get('/deletecontact_status{id}','controlpanelController@deleteContactStatus')->name('delete.contact.status');

//Contact Type
Route::get('/control->contact__type','controlpanelController@ContactType')->name('contact.type');
Route::post('/savecontact_type','controlpanelController@saveContactType')->name('save.contact.type');
Route::post('/updatecontact_type','controlpanelController@updateContactType')->name('update.contact.type');
Route::get('/deletecontact_type{id}','controlpanelController@deleteContactType')->name('delete.contact.type');

//Device Brand
Route::get('/control->device_brand','controlpanelController@deviceBrand')->name('device.brand');
Route::post('/save_device_brand','controlpanelController@saveDeviceBrand')->name('save.device.brand');
Route::post('/update_device_brand','controlpanelController@updateDeviceBrand')->name('update.device.brand');
Route::get('/delete_device_brand{id}','controlpanelController@deleteDeviceBrand')->name('delete.device.brand');

//Gender
Route::get('/control->gender','controlpanelController@gender')->name('gender');
Route::post('/savegender','controlpanelController@saveGender')->name('save.gender');


Route::get('/add_subject','subjectController@addSubject')->name('add.subject');
Route::post('/save_subject','subjectController@saveSubject')->name('save.subject');
Route::get('/subject/{id}','subjectController@subject')->name('subject');

//device Registry
Route::post('/save_device_registry','deviceController@saveDevice')->name('save.device.registry');
Route::post('/update_device_registry/{id}','deviceController@updateDevice')->name('update.device.registry');

//contact Registry
Route::post('save_contact','contactController@saveContact')->name('save.contact');
Route::post('update_contact/{id}','contactController@updateContact')->name('update.contact');
