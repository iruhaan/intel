<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactRegistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_registries', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->date('contact_status_date')->nullable();
            $table->string('contact');

            $table->bigInteger('contact_type_id')->unsigned();
            $table->foreign('contact_type_id')->references('id')->on('contact_types');

            $table->bigInteger('subjects_id')->unsigned();
            $table->foreign('subjects_id')->references('id')->on('subjects');

            $table->bigInteger('device_registry_id')->unsigned()->nullable();
            $table->foreign('device_registry_id')->references('id')->on('device_registries');

            $table->bigInteger('contact_status_id')->unsigned()->nullable();
            $table->foreign('contact_status_id')->references('id')->on('contact_statuses');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_registries');
    }
}
