<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceRegistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_registries', function (Blueprint $table) {
            $table->id();
            $table->string('model')->nullable();
            $table->date('date');
            $table->string('serial_no')->nullable();
            $table->string('IMEI1')->nullable();
            $table->string('IMEI2')->nullable();
            $table->date('status_date')->nullable();

            //foreign Keys
            $table->bigInteger('subjects_id')->unsigned();
            $table->foreign('subjects_id')->references('id')->on('subjects');

            $table->bigInteger('device_brand_id')->unsigned();
            $table->foreign('device_brand_id')->references('id')->on('device_brands');

            $table->bigInteger('device_status_id')->unsigned();
            $table->foreign('device_status_id')->references('id')->on('device_statuses');

            $table->bigInteger('device_type_id')->unsigned();
            $table->foreign('device_type_id')->references('id')->on('device_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_registries');
    }
}
