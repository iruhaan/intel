<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->string('address')->nullable();
            $table->string('NID')->nullable();
            $table->date('DOB')->nullable();
            $table->string('contact')->nullable();
            $table->string('present_address')->nullable();
            $table->string('nick_name')->nullable();

            //foreign Keys
            $table->bigInteger('gender_id')->unsigned();
            $table->foreign('gender_id')->references('id')->on('genders');

            $table->bigInteger('person_type_id')->unsigned();
            $table->foreign('person_type_id')->references('id')->on('person_types');

            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');

            $table->bigInteger('gang_id')->unsigned()->nullable();
            $table->foreign('gang_id')->references('id')->on('gangs');

            $table->bigInteger('security_level_id')->unsigned()->nullable();
            $table->foreign('security_level_id')->references('id')->on('security_levels');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
