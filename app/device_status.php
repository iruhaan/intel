<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device_status extends Model
{
  protected $fillable = [
    'device_status',
  ];
}
