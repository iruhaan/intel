<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact_type extends Model
{
  protected $fillable = [
    'contact_type',
  ];
}
