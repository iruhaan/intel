<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

class information_evaluation extends Model
{
  use Loggable;
  protected $fillable = [
    'information_grade',
    'grade_detail_heading',
    'grade_detail'
  ];
}
