<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact_status extends Model
{

  protected $fillable = [
    'contact_status',
  ];
}
