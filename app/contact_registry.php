<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact_registry extends Model
{
  protected $fillable = [
    'date',
    'contact_type_id',
    'contact',
    'subjects_id',
    'device_registry_id',
    'contact_status_id',
    'contact_status_date',
    ];
  public function device_registry(){
    return $this->hasOne(device_registry::class,'id','device_registry_id')->withDefault();
  }
  public function contact_status(){
    return $this->belongsTo(contact_status::class)->withDefault();
  }
  public function contact_type(){
    return $this->belongsTo(contact_type::class)->withDefault();
  }

}
