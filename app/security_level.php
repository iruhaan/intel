<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class security_level extends Model
{
    use Loggable;
    protected $fillable = [
      'security_level_code',
      'security_level_dhi',
      'security_level_eng'
    ];
}
