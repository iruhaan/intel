<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Carbon\Carbon;

class subjects extends Model
{
  use Loggable;
  protected $fillable = [
    'full_name',
    'address',
    'present_address',
    'NID',
    'DOB',
    'contact',
    'nick_name',
    'person_type_id',
    'country_id',
    'gang_id',
    'gender_id',
    'security_level_id'
  ];


  public function device_registry(){
    return $this->hasMany(device_registry::class);
  }
  public function contact_registry(){
    return $this->hasMany(contact_registry::class);
  }

  public function person_type(){
    return $this->belongsTo(person_type::class);
  }

  public function gender(){
    return $this->belongsTo(gender::class);
  }


  public function security_level(){
    return $this->belongsTo(security_level::class)->withdefault();
  }

  public function gang(){
    return $this->belongsTo(gang::class)->withdefault();
  }

  public function country(){
    return $this->belongsTo(country::class)->withdefault();
  }

   // Accessor for Age.
  public function age(){
      return Carbon::parse($this->attributes['DOB'])->age;
  }

}
