<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class source_evaluation extends Model
{
  use Loggable;
    protected $fillable = [
      'source_grade',
      'grade_detail_heading',
      'grade_detail'
    ];
}
