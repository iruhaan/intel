<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

class country extends Model
{
    use Loggable;
    protected $fillable = [
      'country_dhi',
      'country_eng'
    ];
}
