<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device_registry extends Model
{
  protected $fillable = [
    'subjects_id',
    'device_brand_id',
    'device_status_id',
    'device_type_id',
    'model',
    'date',
    'serial_no',
    'IMEI1',
    'IMEI2',
    'status_date',
  ];

  public function device_type(){
    return $this->belongsTo(device_type::class);
  }

  public function device_brand(){
    return $this->hasOne(device_brand::class,'id','device_brand_id')->withDefault();
  }

  public function device_status(){
    return $this->belongsTo(device_status::class)->withDefault();
  }

}
