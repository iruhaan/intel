<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class gang extends Model
{
  use Loggable;
  protected $fillable = [
    'gang_name_dhi',
    'gang_name_eng'
  ];
}
