<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

class person_type extends Model
{
  use Loggable;
  protected $fillable = [
      'person_type',
      'person_type_eng',
      'person_type_code',
  ];


}
