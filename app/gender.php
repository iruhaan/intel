<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;

class gender extends Model
{
    use Loggable;
    protected $fillable = [
      'gender_eng',
      'gender_dhi',
      'gender_code'
    ];
}
