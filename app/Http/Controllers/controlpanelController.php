<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\person_type;
use App\gang;
use App\country;
use App\information_evaluation;
use App\source_evaluation;
use App\security_level;
use App\source_type;
use App\gender;
use App\device_type;
use App\device_status;
use App\device_brand;
use App\contact_type;
use App\contact_status;

class controlpanelController extends Controller
{
        public function __construct()
        {
            $this->middleware('auth');
        }

        public function index(){
          return view('control.index');
        }
        public function personType(){
          $person_types = person_type::all();
          return view('control.person_type',compact('person_types'));
        }

        public function savePersonType(Request $request){
          person_type::create($request->all());
          return redirect()->back();
        }

        public function updatePersonType(Request $request){
          $person_type = person_type::Find($request->person_type_id);
          $person_type->update($request->all());
          return redirect()->back()->with('loader',true);
        }

        public function deletePersonType($id){
          $person_type = person_type::Find($id);
          $person_type->delete();
          return redirect()->back()->with('loader',true);
        }

        //Gangs

        public function gangName(){
          $gangnames = gang::all();
          return view('control.gang_name',compact('gangnames'));
        }

        public function saveGangName(Request $request){
          gang::create($request->all());
          return redirect()->back();
        }

        public function updateGangName(Request $request){
          $gangnames = gang::Find($request->id);
          $gangnames->update($request->all());
          return redirect()->back()->with('loader',true);
        }

        public function deleteGangName($id){
          $gangnames = gang::Find($id);
          $gangnames->delete();
          return redirect()->back()->with('loader',true);
          }


        //Country
        public function country(){
          $countries = country::all();
          return view('control.country',compact('countries'));
        }

        public function saveCountry(Request $request){
          country::create($request->all());
          return redirect()->back();
        }

        public function updateCountry(Request $request){
          $countries = country::Find($request->id);
          $countries->update($request->all());
          return redirect()->back()->with('loader',true);
        }

        public function deleteCountry($id){
          $countries = country::Find($id);
          $countries->delete();
          return redirect()->back()->with('loader',true);
          }


        //Evaluation
        public function evaluation(){
          $information_evaluations = information_evaluation::all();
          $source_evaluations = source_evaluation::all();
          return view('control.evaluation',compact('source_evaluations','information_evaluations'));
        }

        public function saveInformationEvaluation(Request $request){

          information_evaluation::create($request->all());
          return redirect()->back();
        }

        public function updateInformationEvaluation(Request $request){
          $informationEvaluation = information_evaluation::Find($request->id);
          $informationEvaluation->update($request->all());
          return redirect()->back()->with('loader',true);
        }

        public function deleteInformationEvaluation($id){
          $information_evaluation = information_evaluation::Find($id);
          $information_evaluation->delete();
          return redirect()->back()->with('loader',true);
          }
        //Source
        public function saveSourceEvaluation(Request $request){

          source_evaluation::create($request->all());
          return redirect()->back();
        }

        public function updateSourceEvaluation(Request $request){
          $sourceEvaluation = source_evaluation::Find($request->id);
          $sourceEvaluation->update($request->all());
          return redirect()->back()->with('loader',true);
        }

        public function deleteSourceEvaluation($id){
          $source_evaluation = source_evaluation::Find($id);
          $source_evaluation->delete();
          return redirect()->back()->with('loader',true);
          }

          //Security Level
          public function security_level(){
            $security_levels = security_level::all();

            return view('control.security_level',compact('security_levels'));
          }

          public function saveSecurityLevel(Request $request){

            security_level::create($request->all());
            return redirect()->back();
          }

          public function updateSecurityLevel(Request $request){
            $security_levels = security_level::Find($request->id);
            $security_levels->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteSecurityLevel($id){
            $security_levels = security_level::Find($id);
            $security_levels->delete();
            return redirect()->back()->with('loader',true);
            }

          //Source Type
          public function source_type(){
            $source_types = source_type::all();
            return view('control.type_of_source',compact('source_types'));
          }

          public function saveSourceType(Request $request){
            source_type::create($request->all());
            return redirect()->back();
          }

          public function updateSourceType(Request $request){
            $source_type = source_type::Find($request->id);
            $source_type->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteSourceType($id){
            $source_type = source_type::Find($id);
            $source_type->delete();
            return redirect()->back()->with('loader',true);
            }

          public function gender(){
            $genders = gender::all();
            return view('control.gender',compact('genders'));
            }
          public function saveGender(Request $request){
            gender::create($request->all());
            return redirect()->back();
            }

          public function deviceType(){
            $device_types = device_type::all();
            return view('control.device_type',compact('device_types'));
          }

          public function saveDeviceType(Request $request){
            device_type::create($request->all());
            return redirect()->back();
          }

          public function updateDeviceType(Request $request){
            $device_type = device_type::Find($request->id);
            $device_type->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteDeviceType($id){
            $device_type = device_type::Find($id);
            $device_type->delete();
            return redirect()->back()->with('loader',true);
          }

          public function deviceStatus(){
            $device_statuses = device_status::all();
            return view('control.device_status',compact('device_statuses'));
          }

          public function saveDeviceStatus(Request $request){
            device_status::create($request->all());
            return redirect()->back();
          }

          public function updateDeviceStatus(Request $request){
            $device_status = device_status::Find($request->id);
            $device_status->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteDeviceStatus($id){
            $device_status = device_status::Find($id);
            $device_status->delete();
            return redirect()->back()->with('loader',true);
          }

          public function contactStatus(){
            $contact_statuses = contact_status::all();
            return view('control.contact_status',compact('contact_statuses'));
          }

          public function saveContactStatus(Request $request){
            contact_status::create($request->all());
            return redirect()->back();
          }

          public function updateContactStatus(Request $request){
            $contact_status = contact_status::Find($request->id);
            $contact_status->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteContactStatus($id){
            $contact_status = contact_status::Find($id);
            $contact_status->delete();
            return redirect()->back()->with('loader',true);
          }

          public function contactType(){
            $contact_types = contact_type::all();
            return view('control.contact_type',compact('contact_types'));
          }

          public function saveContactType(Request $request){
            contact_type::create($request->all());
            return redirect()->back();
          }

          public function updateContactType(Request $request){
            $contact_type = contact_type::Find($request->id);
            $contact_type->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteContactType($id){
            $contact_type = contact_type::Find($id);
            $contact_type->delete();
            return redirect()->back()->with('loader',true);
          }

          public function deviceBrand(){
            $device_brandes = device_brand::all();
            return view('control.device_brand',compact('device_brandes'));
          }

          public function saveDeviceBrand(Request $request){
            device_brand::create($request->all());
            return redirect()->back();
          }

          public function updateDeviceBrand(Request $request){
            $device_brand = device_brand::Find($request->id);
            $device_brand->update($request->all());
            return redirect()->back()->with('loader',true);
          }

          public function deleteDeviceBrand($id){
            $device_brand = device_brand::Find($id);
            $device_brand->delete();
            return redirect()->back()->with('loader',true);
          }
}
