<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\contact_registry;
use Redirect;
use URL;

class contactController extends Controller
{
    public function saveContact(Request $request){
      contact_registry::create($request->all());
      return Redirect::to(URL::previous() . "#contact");
    }
    public function updateContact($id, Request $request){
      $contact = contact_registry::Find($id);
      $contact->update($request->all());
      return Redirect::to(URL::previous() . "#contact")->with('success','Change(s) Updated successfully!');
    }
}
