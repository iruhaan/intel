<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\device_registry;
use Redirect;
use URL;

class deviceController extends Controller
{
    public function saveDevice(Request $request){
      $reqDate = $request->date;
      $devReg = device_registry::create($request->all());
      $devReg->status_date = $reqDate;
      $devReg->save();
      return Redirect::to(URL::previous() . "#deviceTag");
    }

    public function updateDevice($id, Request $request){
      $device = device_registry::Find($id);
      $device->update($request->all());
      return Redirect::to(URL::previous() . "#deviceTag");
    }
}
