<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\gender;
use App\country;
use App\gang;
use App\security_level;
use App\person_type;
use App\subjects;
use App\device_type;
use App\device_registry;
use App\device_brand;
use App\device_status;
use App\contact_status;
use App\contact_registry;
use App\contact_type;

class subjectController extends Controller
{
    public function addSubject(){
      $genders = gender::all();
      $countries = country::all();
      $gangs = gang::all();
      $person_types = person_type::all();

      $security_levels = security_level::all();
      return view('subject.add_subject',compact('genders','countries','gangs','security_levels','person_types'));
    }

    public function saveSubject(Request $request){
      subjects::create($request->all());
      return redirect('/home');
    }

    public function subject($id){
      $subject = subjects::Find($id);
      $device_brands = device_brand::all();
      $device_types = device_type::all();
      $device_statuses = device_status::all();
      $contact_types = contact_type::all();
      $contact_types = contact_type::all();
      $device_registries = device_registry::all();
      $contact_statuses = contact_status::all();
      return view('subject.subject_index',compact(
        'subject',
        'device_brands',
        'device_types',
        'device_statuses',
        'device_registries',
        'contact_statuses',
        'contact_types'
      ));
    }

}
