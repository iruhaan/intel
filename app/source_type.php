<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;


class source_type extends Model
{
  use Loggable;
    protected $fillable = [
      'type_of_source',
      'type_of_source_eng',
      'code'
    ];
}
